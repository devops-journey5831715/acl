#!/bin/bash

# Define variables
NFS_SERVER="nfs-server"
NFS_EXPORT_PATH="/export/path"
MOUNT_POINT="/mnt/nfs"
SOURCE_DIR="/path/to/source"
DEST_DIR="$MOUNT_POINT/path/to/destination"
LOG_FILE="/var/log/acl_transfer.log"

# Function to log messages
log() {
    echo "$(date +"%Y-%m-%d %H:%M:%S") - $1" >> $LOG_FILE
}

# Function to check if NFS is mounted
check_mount() {
    mountpoint -q $MOUNT_POINT
}

# Function to mount NFS
mount_nfs() {
    if ! check_mount; then
        log "Mounting NFS share..."
        mount -t nfs4 -o rw,acl $NFS_SERVER:$NFS_EXPORT_PATH $MOUNT_POINT
        if [ $? -ne 0 ]; then
            log "Failed to mount NFS share."
            exit 1
        fi
    else
        log "NFS share already mounted."
    fi
}

# Function to check ACL support
check_acl_support() {
    log "Checking ACL support on source and destination..."
    
    # Check ACL support on source
    touch $SOURCE_DIR/testfile
    setfacl -m u:testuser:rw $SOURCE_DIR/testfile
    if [ $? -ne 0 ]; then
        log "Source directory does not support ACLs."
        rm -f $SOURCE_DIR/testfile
        exit 1
    else
        rm -f $SOURCE_DIR/testfile
    fi

    # Check ACL support on destination
    touch $DEST_DIR/testfile
    setfacl -m u:testuser:rw $DEST_DIR/testfile
    if [ $? -ne 0 ]; then
        log "Destination directory does not support ACLs."
        rm -f $DEST_DIR/testfile
        exit 1
    else
        rm -f $DEST_DIR/testfile
    fi

    log "Both source and destination support ACLs."
}

# Function to transfer files with ACLs
transfer_files() {
    log "Starting file transfer..."
    rsync -aAX --delete $SOURCE_DIR/ $DEST_DIR/
    if [ $? -eq 0 ]; then
        log "File transfer completed successfully."
    else
        log "File transfer failed."
        exit 1
    fi
}

# Function to verify ACLs on destination
verify_acls() {
    log "Verifying ACLs on destination..."
    DIFF=$(diff <(getfacl -R $SOURCE_DIR) <(getfacl -R $DEST_DIR))
    if [ -z "$DIFF" ]; then
        log "ACL verification successful. ACLs match between source and destination."
    else
        log "ACL verification failed. Differences found."
        echo "$DIFF" >> $LOG_FILE
        exit 1
    fi
}

# Main script execution
log "Starting ACL transfer script."

mount_nfs
check_acl_support
transfer_files
verify_acls

log "ACL transfer script completed successfully."
